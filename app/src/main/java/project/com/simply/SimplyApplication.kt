package project.com.simply

import android.app.Application
import org.koin.core.context.startKoin
import project.com.simply.di.appModule

class SimplyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(appModule)
        }
    }
}