package project.com.simply.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import project.com.simply.ui.home.HomeViewModel

val appModule = module {
    viewModel { HomeViewModel() }
}