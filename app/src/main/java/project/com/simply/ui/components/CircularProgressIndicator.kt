package project.com.simply.ui.components


import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import project.com.simply.R


@Composable
fun CircularProgress() {

    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        CircularProgressIndicator(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            color = (colorResource(id = R.color.accentColor))
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ProgressPreview() {
    CircularProgress()
}
