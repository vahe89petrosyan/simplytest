package project.com.simply.ui.home

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.koin.androidx.compose.getViewModel
import project.com.simply.R
import project.com.simply.ui.components.CircularProgress

@Composable
fun ActionView(
    viewModel: HomeViewModel,
    actionViewType: ActionViewType,
    icon: Int? = null,
    title: String = "",
    hasAction: Boolean = false,
    onClick: (() -> Unit)? = null
) {
    val shapeColor = remember { mutableStateOf(Color.Black) }
    val loaderState = viewModel.lockState.collectAsState()
    val disableColor = viewModel.shapeDisableColor.collectAsState()
    val backcolor = viewModel.unlockColor.collectAsState()

    backcolor.let {
        if (hasAction) {
            shapeColor.value = it.value
        }
    }

    disableColor.let {
        if (!hasAction && actionViewType == ActionViewType.DOORS)
            shapeColor.value = it.value
    }


    Column(
        modifier = Modifier
            .wrapContentSize(Alignment.Center)
            .padding(4.dp)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .wrapContentSize(Alignment.Center)
                .size(64.dp)
                .clip(CircleShape)
                .background(shapeColor.value)
                .clickable {
                    onClick?.invoke()

                }

        ) {
            when (actionViewType) {
                ActionViewType.DOORS -> {
                    if (icon != null)
                        Icon(
                            tint = colorResource(id = R.color.white),
                            painter = painterResource(id = icon),
                            contentDescription = ""
                        )
                    if (loaderState.value && hasAction) {
                        CircularProgress()
                    }
                }
                ActionViewType.ENGINE -> {
                    Text(
                        text = title,
                        color = Color.White,
                        textAlign = TextAlign.Center,
                        fontSize = 14.sp
                    )
                }
            }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun ActionViewPreview() {
    ActionView(getViewModel(), ActionViewType.ENGINE, R.drawable.ic_act_lock) {}
}