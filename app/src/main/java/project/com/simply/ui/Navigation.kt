package project.com.simply.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import project.com.simply.ui.home.HomeScreen
import project.com.simply.ui.location.LocationScreen
import project.com.simply.ui.more.MoreScreen
import project.com.simply.ui.vehicle.VehicleScreen

@Composable
fun Navigation(navController: NavHostController) {
    NavHost(navController, startDestination = NavigationItem.Home.route) {
        composable(NavigationItem.Home.route) {
            HomeScreen()
        }
        composable(NavigationItem.Vehicle.route) {
            VehicleScreen()
        }
        composable(NavigationItem.Location.route) {
            LocationScreen()
        }
        composable(NavigationItem.More.route) {
            MoreScreen()
        }

    }
}