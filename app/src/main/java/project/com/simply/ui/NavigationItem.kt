package project.com.simply.ui

import androidx.annotation.StringRes
import project.com.simply.R

sealed class NavigationItem(var route: String, var icon: Int,@StringRes val title: Int) {
    object Home : NavigationItem("home", R.drawable.ic_home, R.string.item_home)
    object Vehicle : NavigationItem("music", R.drawable.ic_car, R.string.item_vehicle)
    object Location : NavigationItem("movies", R.drawable.ic_location, R.string.item_location)
    object More : NavigationItem("books", R.drawable.ic_more, R.string.item_more)
}


