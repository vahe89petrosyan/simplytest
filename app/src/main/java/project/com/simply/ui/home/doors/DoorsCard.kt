package project.com.simply.ui.home.doors

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.androidx.compose.getViewModel
import project.com.simply.R
import project.com.simply.ui.components.CustomAlertDialog
import project.com.simply.ui.home.ActionView
import project.com.simply.ui.home.ActionViewType
import project.com.simply.ui.home.HomeViewModel

@SuppressLint("StateFlowValueCalledInComposition")
@Composable
fun DoorsCard(
    homeViewModel: HomeViewModel
) {
    val statusTitle = homeViewModel.statusTitle.collectAsState()
    val loaderState by homeViewModel.lockState.collectAsState()
    val showDialog = remember { mutableStateOf(loaderState) }
    val coroutineScope = rememberCoroutineScope()


    Column(
        modifier = Modifier
            .padding(8.dp)
            .wrapContentSize()

    ) {
        Row(
            modifier = Modifier
                .padding(start = 4.dp)
                .wrapContentSize(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                fontWeight = FontWeight.Bold,
                text = stringResource(id = R.string.doors),
                color = Color.Black,
                textAlign = TextAlign.Center,
                fontSize = 16.sp,
                modifier = Modifier.padding(end = 2.dp, bottom = 4.dp)

            )
            Divider(
                color = Color.Black.copy(0.6f),
                modifier = Modifier
                    .height(16.dp)
                    .width(1.dp)

            )
            Text(
                text = stringResource(id = statusTitle.value),
                color = Color.Black.copy(0.6f),
                textAlign = TextAlign.Center,
                fontSize = 14.sp,
                modifier = Modifier.padding(start = 2.dp)
            )
        }


        Card(
            elevation = 1.dp,
            shape = RoundedCornerShape(6.dp)
        ) {
            Row(
                modifier = Modifier
                    .background(Color.White)
                    .height(90.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {

                ActionView(homeViewModel, ActionViewType.DOORS, icon = R.drawable.ic_act_lock)
                ActionView(
                    homeViewModel,
                    ActionViewType.DOORS,
                    icon = R.drawable.ic_act_unlock,
                    hasAction = true,
                    onClick = {
                        showDialog.value = true
                    })
            }
        }
    }

    if (showDialog.value && homeViewModel.statusTitle.value == DoorStatus.Locked.value) {
        CustomAlertDialog(showDialog.value,
            stringResource(R.string.unlock_dialog_title),
            stringResource(R.string.unlock_doors_message),
            stringResource(R.string.action_cancel),
            stringResource(R.string.action_confirm_unlock),
            onConfirmRequest = {
                showDialog.value = false
                coroutineScope.launch {
                    withContext(Dispatchers.IO) {
                        homeViewModel.unlockDoors()
                    }
                }
            }, onDismissRequest = {
                showDialog.value = false
            })
    }


}


@Preview(showBackground = true)
@Composable
fun DoorsCardPreview() {
    DoorsCard(getViewModel())
}