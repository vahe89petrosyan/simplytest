package project.com.simply.ui.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.SnackbarResult
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import org.koin.androidx.compose.getViewModel
import project.com.simply.R
import project.com.simply.ui.home.HomeViewModel

@Composable
fun SnackBar(
    infoMessage: String,
    homeViewModel: HomeViewModel
) {
    val sandbarHostState = remember { SnackbarHostState() }

    Box(Modifier.fillMaxSize()) {
        LaunchedEffect(sandbarHostState) {
            val result = sandbarHostState.showSnackbar(
                message = infoMessage
            )
            when (result) {
                SnackbarResult.Dismissed -> {
                    homeViewModel.changeToastStatus()
                }
                else -> {}
            }
        }

        SnackbarHost(

            hostState = sandbarHostState,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = 56.dp))
    }
}

@Preview(showBackground = true)
@androidx.compose.runtime.Composable
fun SnackBarSamplePreview() {
    SnackBar(stringResource(id = R.string.doors_unlocked), getViewModel())
}