package project.com.simply.ui.home

enum class ActionViewType {
    DOORS,
    ENGINE
}