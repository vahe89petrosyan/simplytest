package project.com.simply.ui.components

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import project.com.simply.R


@Composable
fun CustomAlertDialog(
    showDialog: Boolean,
    title: String,
    message: String,
    cancelText: String,
    confirmText: String,
    onConfirmRequest: () -> Unit,
    onDismissRequest: () -> Unit

) {

    MaterialTheme {
        if (showDialog) {
            AlertDialog(
                onDismissRequest = {},
                title = {
                    Text(text = title)
                },
                text = {
                    Text(message)
                },
                confirmButton = {
                    Button(
                        onClick = {
                            onConfirmRequest()
                        }) {
                        Text(confirmText)
                    }
                },
                dismissButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color.White,
                            contentColor = Color.Black
                        ),
                        onClick = {
                            onDismissRequest()
                        }) {
                        Text(cancelText)

                    }
                }
            )
        }
    }


}

@Preview(showBackground = true)
@Composable
fun DialogPreview() {
    CustomAlertDialog(true,
        stringResource(R.string.unlock_dialog_title),
        stringResource(R.string.unlock_doors_message),
        stringResource(R.string.action_cancel),
        stringResource(R.string.action_confirm_unlock),
        onDismissRequest = {},
        onConfirmRequest = {})
}
