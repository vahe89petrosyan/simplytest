package project.com.simply.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.koin.androidx.compose.getViewModel
import project.com.simply.R
import project.com.simply.ui.components.SnackBar
import project.com.simply.ui.home.doors.DoorsCard
import project.com.simply.ui.home.engine.EngineCard

@Composable
fun HomeScreen(homeViewModel: HomeViewModel = getViewModel()) {
    val showToast = homeViewModel.showToast.collectAsState()

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 16.dp)
            .background(colorResource(id = R.color.colorPrimaryDark))
            .wrapContentSize(Alignment.TopCenter)
    ) {
        Row(
            modifier = Modifier
                .height(56.dp)
                .background(Color.White)
                .fillMaxSize(),

            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(id = R.string.car_name),
                color = Color.Black,
                textAlign = TextAlign.Center,
                fontSize = 25.sp,
                modifier = Modifier
                    .padding(8.dp)


            )
            Divider(
                color = colorResource(id = R.color.accentColor),
                modifier = Modifier
                    .height(24.dp)
                    .width(2.dp)

            )
            Icon(
                painter = painterResource(id = R.drawable.ic_notif_gas),
                contentDescription = ""

            )
            Text(
                text = stringResource(id = R.string.car_ml),
                color = Color.Black,
                textAlign = TextAlign.Center,
                fontSize = 16.sp
            )
        }

        Row(
            modifier = Modifier
                .padding(top = 24.dp, bottom = 24.dp)
                .wrapContentSize(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {

            Icon(
                tint = colorResource(id = R.color.accentColor),
                painter = painterResource(id = R.drawable.ic_btn_refresh),
                contentDescription = ""
            )
            Text(
                text = stringResource(id = R.string.update_time),
                color = Color.Black,
                textAlign = TextAlign.Center,
                fontSize = 16.sp
            )
        }

        Image(

            painter = painterResource(id = R.drawable.car_qx55),
            contentDescription = "",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )


        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(top = 24.dp, bottom = 24.dp)
                .wrapContentSize()
        ) {

            DoorsCard(homeViewModel)

            EngineCard(homeViewModel)

        }

        if (showToast.value) {
            SnackBar(stringResource(id = R.string.doors_unlocked),homeViewModel)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    HomeScreen()
}