package project.com.simply.ui.home

import android.os.CountDownTimer
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import project.com.simply.ui.home.doors.DoorStatus

class HomeViewModel : ViewModel() {

    private val _lockState = MutableStateFlow(false)
    val lockState = _lockState.asStateFlow()

    private val _shapeDisableColor = MutableStateFlow(Color.Black)
    val shapeDisableColor = _shapeDisableColor.asStateFlow()

    private val _statusTitle = MutableStateFlow(DoorStatus.Locked.value)
    val statusTitle = _statusTitle.asStateFlow()

    private val _showToast = MutableStateFlow(false)
    val showToast = _showToast.asStateFlow()

    private val _unlockColor = MutableStateFlow(Color.Black)
    val unlockColor = _unlockColor.asStateFlow()


    private val timer = object : CountDownTimer(5000, 1000) {
        override fun onTick(millisUntilFinished: Long) {

        }

        override fun onFinish() {
            _lockState.value = false
            _statusTitle.value = DoorStatus.Unlocked.value
            _shapeDisableColor.value = Color.Black
            _showToast.value = true
            _unlockColor.value = Color(0xFFA66A53)
        }
    }

    fun unlockDoors() {
        _lockState.value = true
        _statusTitle.value = DoorStatus.Unlocking.value
        _shapeDisableColor.value = Color.Gray
        _unlockColor.value = Color.White

        timer.start()
    }

    fun changeToastStatus() {
        _showToast.value = false
    }
}