package project.com.simply.ui.home.doors

import androidx.annotation.StringRes
import project.com.simply.R

enum class DoorStatus(@StringRes val value: Int) {
    Locked(R.string.status_locked), Unlocked(R.string.status_unlocked), Unlocking(R.string.status_unlocking);
}

